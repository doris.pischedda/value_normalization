% function start_matlabbatch(matlabbatch, identifier)
% Running configured matlabbatches
%
% author: Doris Pischedda
%  start: 2017/11/01


function start_matlabbatch(matlabbatch, identifier)

%% initialize spm jobman
display('Initializing spm_jobman')
spm_jobman('initcfg')

%% run batches
for mbatch_ind = 1:length(matlabbatch)
    curr_matlabbatch = matlabbatch(mbatch_ind);
    spm_jobman('run', curr_matlabbatch);
    display('start_matlabbatch: Job finished successfully')
end

%% End message
display(sprintf('start_matlabbatch: ALL JOBS FINISHED SUCESSFULLY (%i jobs) for %s', mbatch_ind, identifier));