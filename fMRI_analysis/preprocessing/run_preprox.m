% Function to run the preprocessing for all subjects

% author: Doris Pischedda
%  start: 2017/08/07

% Set subject list
substodo = [1:28];

% Run preprocessing
for sbj = substodo
    preprox_valnorm(sbj)
    close all 
end