% Function to perform the preprocessing on fMRI data from Palminteri et al., 2015

% author: Doris Pischedda
%  start: 2017/08/07

function preprox_valnorm(sbj)

% prebatch processing

% number of runs
nrun = 4;

% runn defines which steps we want to do
runn = ones(1,6);

baseDir = sprintf('Z:\\CORGIO003M9I\\valueNormalization_MVPA\\02_Data\\Sbj%02i\\NIFTI\\', sbj); % base folder

structDir = sprintf('%s%s', baseDir, 'T1'); % folder where the structural image is stored
spmDir = 'C:\\Users\\doris.pischedda\\Documents\\Postdoc\\spm12\\'; % path to spm

% create baseDir
mkdir(baseDir) 
 
for run_ind = 1:nrun
    run_subdir{run_ind} = sprintf('run%02i\\', run_ind);
    run_dir{run_ind} = fullfile(baseDir, run_subdir{run_ind});
end

%% Add paths 
addpath(spmDir); % Add path to spm folder
addpath(genpath('Z:\\CORGIO003M9I\\valueNormalization_MVPA\\Analysis\\preprocessing')) % Add script folder

%% change to log directory (to save ps files there)
logdir = fullfile(baseDir, 'log');
mkdir(logdir)
cd(logdir)

%% start spm fmri
spm fmri

batn=0; % initialise matlabbatch counter
spm_jobman('initcfg'); % initialise SPM batch mode

%% Spatial realignment

if runn(1)==1; 
    clear matlabbatch; 
    batn=0; 
end;
batn=batn+1;

for i = 1:nrun
    matlabbatch{batn}.spm.spatial.realignunwarp.data(i).scans = cellstr(spm_select('FPList', run_dir{i}, '^f.*\.nii$')) ; % selects functional images
    matlabbatch{batn}.spm.spatial.realignunwarp.data(i).pmscan = '';    
end;

matlabbatch{batn}.spm.spatial.realignunwarp.eoptions.quality = 1;
matlabbatch{batn}.spm.spatial.realignunwarp.eoptions.sep = 4;
matlabbatch{batn}.spm.spatial.realignunwarp.eoptions.fwhm = 5;
matlabbatch{batn}.spm.spatial.realignunwarp.eoptions.rtm = 1;
matlabbatch{batn}.spm.spatial.realignunwarp.eoptions.einterp = 7;
matlabbatch{batn}.spm.spatial.realignunwarp.eoptions.ewrap = [0 0 0];
matlabbatch{batn}.spm.spatial.realignunwarp.eoptions.weight = '';
matlabbatch{batn}.spm.spatial.realignunwarp.uweoptions.basfcn = [12 12];
matlabbatch{batn}.spm.spatial.realignunwarp.uweoptions.regorder = 1;
matlabbatch{batn}.spm.spatial.realignunwarp.uweoptions.lambda = 100000;
matlabbatch{batn}.spm.spatial.realignunwarp.uweoptions.jm = 0;
matlabbatch{batn}.spm.spatial.realignunwarp.uweoptions.fot = [4 5];
matlabbatch{batn}.spm.spatial.realignunwarp.uweoptions.sot = [];
matlabbatch{batn}.spm.spatial.realignunwarp.uweoptions.uwfwhm = 4;
matlabbatch{batn}.spm.spatial.realignunwarp.uweoptions.rem = 1;
matlabbatch{batn}.spm.spatial.realignunwarp.uweoptions.noi = 5;
matlabbatch{batn}.spm.spatial.realignunwarp.uweoptions.expround = 'Average';
matlabbatch{batn}.spm.spatial.realignunwarp.uwroptions.uwwhich = [2 1];
matlabbatch{batn}.spm.spatial.realignunwarp.uwroptions.rinterp = 7;
matlabbatch{batn}.spm.spatial.realignunwarp.uwroptions.wrap = [0 0 0];
matlabbatch{batn}.spm.spatial.realignunwarp.uwroptions.mask = 1;
matlabbatch{batn}.spm.spatial.realignunwarp.uwroptions.prefix = 'u';

if runn(1)==1;
    spm_jobman('run',matlabbatch);
end;

%% Slice time correction

if runn(2)==1; 
    clear matlabbatch; 
    batn=0; 
end;
batn=batn+1;

for i = 1:nrun
    matlabbatch{batn}.spm.temporal.st.scans{1,i} = cellstr(spm_select('FPList', run_dir{i}, '^uf.*\.nii$')); % selects realigned images
end;

tr = 2.2;
nslices = 47;
matlabbatch{batn}.spm.temporal.st.nslices = nslices;
matlabbatch{batn}.spm.temporal.st.tr = tr;
matlabbatch{batn}.spm.temporal.st.ta = tr - tr/nslices; % TE = TR - TR/nslices
matlabbatch{batn}.spm.temporal.st.so = [1:2:nslices 2:2:nslices]; % interleaved acquisition order
matlabbatch{batn}.spm.temporal.st.refslice = 23;
matlabbatch{batn}.spm.temporal.st.prefix = 'a';

if runn(2)==1;
    spm_jobman('run',matlabbatch);
end;

%% Coregistration to T1

if runn(3)==1; 
    clear matlabbatch; 
    batn=0; 
end;
batn=batn+1;

matlabbatch{batn}.spm.spatial.coreg.estimate.ref(1) = cellstr(spm_select('FPList', run_dir{1}, '^meanuf.*\.nii$')); % selects mean file from realignment
matlabbatch{batn}.spm.spatial.coreg.estimate.source = cellstr(spm_select('FPList', structDir, '^s.*.nii$')); % selects structural image
matlabbatch{batn}.spm.spatial.coreg.estimate.other = {''};
matlabbatch{batn}.spm.spatial.coreg.estimate.eoptions.cost_fun = 'nmi';
matlabbatch{batn}.spm.spatial.coreg.estimate.eoptions.sep = [4 2];
matlabbatch{batn}.spm.spatial.coreg.estimate.eoptions.tol = [0.02 0.02 0.02 0.001 0.001 0.001 0.01 0.01 0.01 0.001 0.001 0.001];
matlabbatch{batn}.spm.spatial.coreg.estimate.eoptions.fwhm = [7 7];

if runn(3)==1;
    spm_jobman('run',matlabbatch);
end;

%% Segmentation 

if runn(4)==1; 
    clear matlabbatch; 
    batn=0; 
end;
batn=batn+1;

matlabbatch{batn}.spm.spatial.preproc.channel.vols = cellstr(spm_select('FPList', structDir, '^s.*.nii$')); % selects structural image
matlabbatch{batn}.spm.spatial.preproc.channel.biasreg = 0.001;
matlabbatch{batn}.spm.spatial.preproc.channel.biasfwhm = 60;
matlabbatch{batn}.spm.spatial.preproc.channel.write = [0 1];
matlabbatch{batn}.spm.spatial.preproc.tissue(1).tpm =  {[spmDir 'tpm\\TPM.nii,1']};
matlabbatch{batn}.spm.spatial.preproc.tissue(1).ngaus = 1;
matlabbatch{batn}.spm.spatial.preproc.tissue(1).native = [1 0];
matlabbatch{batn}.spm.spatial.preproc.tissue(1).warped = [0 0];
matlabbatch{batn}.spm.spatial.preproc.tissue(2).tpm = {[spmDir 'tpm\\TPM.nii,2']};
matlabbatch{batn}.spm.spatial.preproc.tissue(2).ngaus = 1;
matlabbatch{batn}.spm.spatial.preproc.tissue(2).native = [1 0];
matlabbatch{batn}.spm.spatial.preproc.tissue(2).warped = [0 0];
matlabbatch{batn}.spm.spatial.preproc.tissue(3).tpm = {[spmDir 'tpm\\TPM.nii,3']};
matlabbatch{batn}.spm.spatial.preproc.tissue(3).ngaus = 2;
matlabbatch{batn}.spm.spatial.preproc.tissue(3).native = [1 0];
matlabbatch{batn}.spm.spatial.preproc.tissue(3).warped = [0 0];
matlabbatch{batn}.spm.spatial.preproc.tissue(4).tpm = {[spmDir 'tpm\\TPM.nii,4']};
matlabbatch{batn}.spm.spatial.preproc.tissue(4).ngaus = 3;
matlabbatch{batn}.spm.spatial.preproc.tissue(4).native = [1 0];
matlabbatch{batn}.spm.spatial.preproc.tissue(4).warped = [0 0];
matlabbatch{batn}.spm.spatial.preproc.tissue(5).tpm = {[spmDir 'tpm\\TPM.nii,5']};
matlabbatch{batn}.spm.spatial.preproc.tissue(5).ngaus = 4;
matlabbatch{batn}.spm.spatial.preproc.tissue(5).native = [1 0];
matlabbatch{batn}.spm.spatial.preproc.tissue(5).warped = [0 0];
matlabbatch{batn}.spm.spatial.preproc.tissue(6).tpm = {[spmDir 'tpm\\TPM.nii,6']};
matlabbatch{batn}.spm.spatial.preproc.tissue(6).ngaus = 2;
matlabbatch{batn}.spm.spatial.preproc.tissue(6).native = [0 0];
matlabbatch{batn}.spm.spatial.preproc.tissue(6).warped = [0 0];
matlabbatch{batn}.spm.spatial.preproc.warp.mrf = 1;
matlabbatch{batn}.spm.spatial.preproc.warp.cleanup = 1;
matlabbatch{batn}.spm.spatial.preproc.warp.reg = [0 0.001 0.5 0.05 0.2];
matlabbatch{batn}.spm.spatial.preproc.warp.affreg = 'mni';
matlabbatch{batn}.spm.spatial.preproc.warp.fwhm = 0;
matlabbatch{batn}.spm.spatial.preproc.warp.samp = 3;
matlabbatch{batn}.spm.spatial.preproc.warp.write = [1 1];

if runn(4)==1;
    spm_jobman('run',matlabbatch);
end;


%% Normalisation (write)

if runn(5)==1; 
    clear matlabbatch; 
    batn=0; 
end;
batn=batn+1;

matlabbatch{batn}.spm.spatial.normalise.write.subj.def(1) = cellstr(spm_select('FPList', structDir, '^y_.*\.nii$')); % selects structural file for normalisation from previous step 

files={};
for i =1:nrun
    file{1} = cellstr(spm_select('FPList', run_dir{i}, '^auf.*\.nii$')); % selects realigned and slice-time corrected files 
    files=[files;file{1}];
end;
files = [files; cellstr(spm_select('FPList', run_dir{1}, '^meanuf.*\.nii$'))]; % selects mean file from realignment
matlabbatch{batn}.spm.spatial.normalise.write.subj.resample = files;

matlabbatch{batn}.spm.spatial.normalise.write.woptions.bb = [-78 -112 -70
78 76 85];
matlabbatch{batn}.spm.spatial.normalise.write.woptions.vox = [3 3 3];
matlabbatch{batn}.spm.spatial.normalise.write.woptions.interp = 7;

if runn(5)==1;
    spm_jobman('run',matlabbatch);
end;

%% Smoothing

if runn(6)==1; 
    clear matlabbatch; 
    batn=0; 
end;
batn=batn+1;

for i = 1:nrun
    matlabbatch{batn}.spm.spatial.smooth.data = cellstr(spm_select('FPList', run_dir{i}, '^wauf.*\.nii$')); % selects normalised files
    matlabbatch{batn}.spm.spatial.smooth.fwhm = [6 6 6];
    matlabbatch{batn}.spm.spatial.smooth.dtype = 0;
    matlabbatch{batn}.spm.spatial.smooth.im = 0;
    matlabbatch{batn}.spm.spatial.smooth.prefix = 's';
    batn=batn+1;
end;

if runn(6)==1;
     spm_jobman('run',matlabbatch);
end;

disp('Batch finished')

end