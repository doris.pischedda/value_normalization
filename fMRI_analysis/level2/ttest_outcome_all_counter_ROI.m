%% Computes t-test: positive vs. negative counterfactual outcome in complete feedback trials

%  author: Doris Pischedda
%   start: 2019/12/11


% Load the result of all subjects (they are in the small matfiles)
clear all

substodo = [1:28]; % add all subjects here

n_rois = 18; % number of rois

trainA{1} = 'GainRalt_GainNalt';
trainA{2} = 'GainRalt_LossPalt';
trainA{3} = 'LossNalt_GainNalt';
trainA{4} = 'LossNalt_LossPalt';

basedir = '/analysis/doris/ValNorm/design/outcome_all_counter_HRF_ROIs_train_';


for sbj_ind = 1:length(substodo) 

    for i = 1:length(trainA)
        clear resFile
        resFile = sprintf([basedir trainA{i} '/ROI/sub%02i_svm_outcome_all_counter_HRF_ROIs_train_' trainA{i} '_r4_res_accuracy_minus_chance.mat'], sbj_ind);
        load(resFile)
        for ROIind = 1:n_rois
            data{sbj_ind}.acc(ROIind,i) = results.accuracy_minus_chance.output(ROIind);
        end
    end
    
    meanSbj = mean(data{sbj_ind}.acc, 2);
    resAcc(sbj_ind,:) = meanSbj';
        
end

[H, p, CI, stats] = ttest(resAcc)


% Save to csv file
display('Save to csv file')
dlmwrite('/analysis/doris/ValNorm/Analysis/ROI/ROI_decoding/complete_counterfactual_all_multi_ROI_decoding.csv', resAcc, 'Delimiter', ';')