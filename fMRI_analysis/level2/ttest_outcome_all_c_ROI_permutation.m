%% Computes permutation tests: positive vs. negative factual outcome in complete feedback trials

%  author: Doris Pischedda
%   start: 2019/12/19


% Load the result of all subjects (they are in the small matfiles)
clear all

substodo = [1:28]; % add all subjects here

n_rois = 18; % number of ROIs

n_perm = 8; % number of label permutations

bilROI = n_rois/2; % number of ROIs averaged

trainA{1} = 'GainRfactC_GainNfactC';
trainA{2} = 'GainRfactC_LossPfactC';
trainA{3} = 'LossNfactC_GainNfactC';
trainA{4} = 'LossNfactC_LossPfactC';

basedir = '/analysis/doris/ValNorm/design/outcome_all_c_HRF_ROIs_train_';

for sbj_ind = 1:length(substodo) 
 
    for iPerm = 1:n_perm
        clear meanSbj
        clear resMeanSbj
        clear avROI
            for i = 1:length(trainA)
                clear resFile
                resFile = sprintf([basedir trainA{i} '/ROI/perm/' trainA{i} '/perm%04i_sbj%02i_accuracy_minus_chance.mat'], iPerm, sbj_ind);
                load(resFile)
                       
                for ROIind = 1:n_rois     
                    data{sbj_ind}.acc(ROIind,i) = results.accuracy_minus_chance.output(ROIind);        
                end
            end
            
            meanSbj = mean(data{sbj_ind}.acc, 2);
                                    
            resMeanSbj = reshape(meanSbj', 2, bilROI);
            avROI = mean(resMeanSbj);
            for iROI = 1:bilROI
                a(iROI,sbj_ind, iPerm) = avROI(iROI);
            end
    end        
end
   
P2 = 1e6; % number of permutation tests
alpha = 0.05; % alpha level for test

[results_perm, params] = permutationSecondLevelMeanCore(a, P2, alpha)

% Save results
save('results_outcome_all_c_ROI_perm.mat', 'results_perm')