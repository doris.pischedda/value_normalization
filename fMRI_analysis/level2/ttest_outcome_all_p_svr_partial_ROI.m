%% Computes t-test: partially-adaptive model for factual outcome in partial feedback trials

%  author: Doris Pischedda
%   start: 2019/12/10


% Load the result of all subjects (they are in the small matfiles)
clear all

substodo = [1:28]; % add all subjects here

n_rois = 18; % number of rois

basedir = '/analysis/doris/ValNorm/design/outcome_all_p_HRF_svr_partial_ROIs_ncls4';


for sbj_ind = 1:length(substodo) 

    clear resFile
    resFile = sprintf([basedir '/ROI/sub%02i_svm_outcome_all_p_HRF_svr_partial_ROIs_ncls4_r4_res_zcorr.mat'], sbj_ind);
    load(resFile)
    for ROIind = 1:n_rois
        resZcorr(sbj_ind, ROIind) = results.zcorr.output(ROIind);
    end
        
end

[H, p, CI, stats] = ttest(resZcorr)

% Save to csv file
display('Save to csv file')
dlmwrite('/analysis/doris/ValNorm/Analysis/ROI/ROI_decoding/partial_factual_all_svr_partial_ROI_decoding.csv', resZcorr, 'Delimiter', ';')