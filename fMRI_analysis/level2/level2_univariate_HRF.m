%% Standard batch for second level t-tests on univariate first level HRF models

%  author: Doris Pischedda
%   start: 2017/11/06

clear all
close all

%% Define subjects

subjectList = [1:28]; 

% Add SPM
addpath('/analysis/doris/spm12/');

%% Set directory and model

baseDir = '/analysis/doris/ValNorm/design/univariate';
% resDir = 'ANOVA_good_bad_c';
% resDir = 'ANOVA_good_bad_p';
resDir = 'ANOVA_good_bad_counter';
modelDir = 'outcome_all_HRF';

typeDir ='univariate';
designDir = fullfile(baseDir, modelDir, resDir);
% contrast = 1; % Good > Bad partial factual
% contrast = 2; % Good > Bad complete factual
contrast = 3; % Good > Bad complete counterfactual

%% Prepare batch

% start spm
spm fmri

% Initialise batch
batch_ind = 1;
matlabbatch = {};

matlabbatch{batch_ind}.spm.stats.factorial_design.dir = {designDir};

disp(['Creating target directory ' matlabbatch{batch_ind}.spm.stats.factorial_design.dir{1}])
mkdir(matlabbatch{batch_ind}.spm.stats.factorial_design.dir{1})
    
files = {};

for sub = 1:length(subjectList)
    sbj = subjectList(sub);
    rootDir = sprintf('/analysis/doris/ValNorm/02_Data/Sbj%02i/level1/', sbj); 
    fileDir = fullfile(rootDir, modelDir, typeDir);      
    fileName = sprintf('sess_%04i.nii', contrast);         
        
    currFile = fullfile(fileDir, fileName);
            
    % Check that we have the expected file
    if isempty(currFile)    
        warning('Could not find image %s/%s for sub %i', fileDir, fileName, sbj)               
        if ~exist('failed_subs', 'var')                                                          
            failed_subs = [];                                
        end        
        failed_subs(end + 1, :) = [sbj];        
        fail_execution = 1        
    end
        
    if size(currFile, 1) > 1    
        warning(['Image %s/%s for sub %i is not unique, please check'], fileDir, fileName, sbj)    
    fail_execution = 1              
    end
    
    % add file    
    files{end+1, 1} = currFile
              
end

matlabbatch{1}.spm.stats.factorial_design.des.t1.scans = files;

if exist('fail_execution', 'var')    
    if fail_execution,            
        disp(failed_subs)        
        error('Some files could not be found')        
    end
end
    
% Setting other parameters
matlabbatch{batch_ind}.spm.stats.factorial_design.cov = struct('c', {}, 'cname', {}, 'iCFI', {}, 'iCC', {});
matlabbatch{batch_ind}.spm.stats.factorial_design.masking.tm.tm_none = 1;
matlabbatch{batch_ind}.spm.stats.factorial_design.masking.im = 0;
matlabbatch{batch_ind}.spm.stats.factorial_design.masking.em = {'/analysis/doris/ValNorm/Analysis/level2/Mask/mask_sum_t2.img,1'};
matlabbatch{batch_ind}.spm.stats.factorial_design.globalc.g_omit = 1;
matlabbatch{batch_ind}.spm.stats.factorial_design.globalm.gmsca.gmsca_no = 1;
matlabbatch{batch_ind}.spm.stats.factorial_design.globalm.glonorm = 1;

%% Estimate model

batch_ind = batch_ind + 1;

matlabbatch{batch_ind}.spm.stats.fmri_est.spmmat(1) = cfg_dep;
matlabbatch{batch_ind}.spm.stats.fmri_est.spmmat(1).tname = 'Select SPM.mat';
matlabbatch{batch_ind}.spm.stats.fmri_est.spmmat(1).tgt_spec{1}(1).name = 'filter';
matlabbatch{batch_ind}.spm.stats.fmri_est.spmmat(1).tgt_spec{1}(1).value = 'mat';
matlabbatch{batch_ind}.spm.stats.fmri_est.spmmat(1).tgt_spec{1}(2).name = 'strtype';
matlabbatch{batch_ind}.spm.stats.fmri_est.spmmat(1).tgt_spec{1}(2).value = 'e';
matlabbatch{batch_ind}.spm.stats.fmri_est.spmmat(1).sname = 'Factorial design specification: SPM.mat File';
matlabbatch{batch_ind}.spm.stats.fmri_est.spmmat(1).src_exbranch = substruct('.','val', '{}',{1}, '.','val', '{}',{1}, '.','val', '{}',{1});
matlabbatch{batch_ind}.spm.stats.fmri_est.spmmat(1).src_output = substruct('.','spmmat');
matlabbatch{batch_ind}.spm.stats.fmri_est.method.Classical = 1;
     
%% Run batch
if runjob
    spm_jobman('run',matlabbatch);
end

%% End
display('Level 2 finished')