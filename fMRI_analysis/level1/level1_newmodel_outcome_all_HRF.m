%% First level HRF model for analyses on factual outcomes in trials with partial info and on factual and counterfactual outcomes in trials with complete info  

%  author: Doris Pischedda
%   start: 2018/01/23


function level1_newmodel_outcome_all_HRF(job_nr)

%% Set paths

addpath('/analysis/doris/spm12/');
addpath('/analysis/doris/ValNorm/Analysis/level1')
display(['Starting script for subject ' num2str(job_nr)])

%% Run first level script
     
display(['Running script for subject ' num2str(job_nr)])
            
%% PUT MODEL SPECIFICATION OR CONDITION SPECIFICATION HERE
cfg.design.name = 'outcome_all'; % name of getTimingsXXX file; also name under which your model will be saved

cfg.filter = 'swauf*'; % comment line for decoding (uses default 'auf*')
cfg.univariate_analysis = 1; % 1: to do univariate analysis with normalised images, 0: 1st level for decoding with not normalized images
        
matlabbatch = first_level_valNorm(job_nr, cfg, cfg.design.name);
        
display(['Finished script for subject ' num2str(job_nr)])

end
