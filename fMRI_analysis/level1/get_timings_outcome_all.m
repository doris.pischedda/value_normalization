%% Extracting timings from TestSubXX_SessionY scannerlog

%  author: Doris Pischedda
%   start: 2018/01/23

% For outcome decoding.
% Names of conditions
%         'GainRfactP' : onsets for outcome info in gain trials with partial information when gaining +0.5
%         'GainNfactP' : onsets for outcome info in gain trials with partial information when gaining +0.0
%         'LossNfactP' : onsets for outcome info in loss trials with partial information when losing -0.0
%         'LossPfactP' : onsets for outcome info in loss trials with partial information when losing -0.5
%         'GainRfactC' : onsets for outcome info in gain trials with complete information when gaining +0.5
%         'GainNfactC' : onsets for outcome info in gain trials with complete information when gaining +0.0
%         'LossNfactC' : onsets for outcome info in loss trials with complete information when losing -0.0
%         'LossPfactC' : onsets for outcome info in loss trials with complete information when losing -0.5
%         'GainRalt' : onsets for outcome info in gain trials with complete information when counterfactual outcome is +0.5
%         'GainNalt' : onsets for outcome info in gain trials with complete information when counterfactual outcome is +0.0
%         'LossNalt' : onsets for outcome info in loss trials with complete information when counterfactual outcome is -0.0
%         'LossPalt' : onsets for outcome info in loss trials with complete information when counterfactual outcome is -0.5


function out = get_timings_outcome_all(sbj)   

run_num = 4; 

 % loop over runs
    for iRun = 1:run_num
        clear data
        clear money
        clear stimuli
        clear T0time
      
        % Initialize condition and trial-number vectors         
        GainRfactP = [];             
        GainNfactP = [];   
        LossNfactP = [];             
        LossPfactP = [];  
        GainRfactC = [];             
        GainNfactC = [];   
        LossNfactC = [];             
        LossPfactC = []; 
        GainRalt = [];             
        GainNalt = [];   
        LossNalt = [];             
        LossPalt = []; 
        trialnr_GainRfactP = [];     
        trialnr_GainNfactP = [];     
        trialnr_LossNfactP = [];     
        trialnr_LossPfactP = [];
        trialnr_GainRfactC = [];     
        trialnr_GainNfactC = [];     
        trialnr_LossNfactC = [];     
        trialnr_LossPfactC = [];
        trialnr_GainRalt = [];     
        trialnr_GainNalt = [];     
        trialnr_LossNalt = [];     
        trialnr_LossPalt = [];
        
        fname = sprintf('/analysis/doris/ValNorm/01_Logfiles/TestSub%i_Session%i.mat', sbj, iRun);

        load(fname)
               
        nrTrials = length(data);  
        dummies = 2.2 * 5; % dummy scans
        delayFeed = 3.5; % onset for outcome is 3.5s after context presentation

        % construct onset vectors
        for iTrial = 1:nrTrials                          
        
            % extract outcome onsets for gain trials (partial)
            if data(iTrial, 4) == 1  
                
                if data(iTrial, 9) == 1 % for reward outcome (+0.5)
                    % get time in seconds 
                    time = ((data(iTrial, 6) - T0time)/1000) + dummies + delayFeed;   
                    GainRfactP = [GainRfactP time];     
                    trialnr_GainRfactP = [trialnr_GainRfactP iTrial];
                    
                elseif data(iTrial, 9) == 0 % for null outcome (+0.0)
                    time = ((data(iTrial, 6) - T0time)/1000) + dummies + delayFeed; 
                    GainNfactP = [GainNfactP time];     
                    trialnr_GainNfactP = [trialnr_GainNfactP iTrial];
                end
                
            % extract outcome onsets for gain trials (complete)
            elseif data(iTrial, 4) == 2   
                  
                % for factual outcome
                if data(iTrial, 9) == 1 % for positive outcome (+0.5)
                    % get time in seconds 
                    time = ((data(iTrial, 6) - T0time)/1000) + dummies + delayFeed;   
                    GainRfactC = [GainRfactC time];     
                    trialnr_GainRfactC = [trialnr_GainRfactC iTrial];
                    
                elseif data(iTrial, 9) == 0 % for negative outcome (+0.0)
                    time = ((data(iTrial, 6) - T0time)/1000) + dummies + delayFeed; 
                    GainNfactC = [GainNfactC time];     
                    trialnr_GainNfactC = [trialnr_GainNfactC iTrial];
                end          
                
                % for counterfactual outcome
                if data(iTrial, 10) == 1 % for positive outcome (+0.5)
                    % get time in seconds 
                    time = ((data(iTrial, 6) - T0time)/1000) + dummies + delayFeed;   
                    GainRalt = [GainRalt time];     
                    trialnr_GainRalt = [trialnr_GainRalt iTrial];
                    
                elseif data(iTrial, 10) == 0 % for negative outcome (+0.0)
                    time = ((data(iTrial, 6) - T0time)/1000) + dummies + delayFeed; 
                    GainNalt = [GainNalt time];     
                    trialnr_GainNalt = [trialnr_GainNalt iTrial];
                end
                
            % extract outcome onsets for loss trials (partial)
            elseif data(iTrial, 4) == 3    
               
                if data(iTrial, 9) == 1 % for null outcome (-0.0)
                    % get time in seconds 
                    time = ((data(iTrial, 6) - T0time)/1000) + dummies + delayFeed;   
                    LossNfactP = [LossNfactP time];     
                    trialnr_LossNfactP = [trialnr_LossNfactP iTrial];
                    
                elseif data(iTrial, 9) == 0 % for punishment outcome (-0.5)
                    time = ((data(iTrial, 6) - T0time)/1000) + dummies + delayFeed; 
                    LossPfactP = [LossPfactP time];   
                    trialnr_LossPfactP = [trialnr_LossPfactP iTrial];
                end
            
            % extract outcome onsets for loss trials (complete)
            elseif data(iTrial, 4) == 4 
                
                % for factual outcome
                if data(iTrial, 9) == 1 % for negative outcome (-0.0)
                    % get time in seconds 
                    time = ((data(iTrial, 6) - T0time)/1000) + dummies + delayFeed;   
                    LossNfactC = [LossNfactC time];     
                    trialnr_LossNfactC = [trialnr_LossNfactC iTrial];
                    
                elseif data(iTrial, 9) == 0 % for positive outcome (-0.5)
                    time = ((data(iTrial, 6) - T0time)/1000) + dummies + delayFeed; 
                    LossPfactC = [LossPfactC time];     
                    trialnr_LossPfactC = [trialnr_LossPfactC iTrial];
                end
                
                % for counterfactual outcome
                if data(iTrial, 10) == 1 % for negative outcome (-0.0)
                    % get time in seconds 
                    time = ((data(iTrial, 6) - T0time)/1000) + dummies + delayFeed;   
                    LossNalt = [LossNalt time];     
                    trialnr_LossNalt = [trialnr_LossNalt iTrial];
                    
                elseif data(iTrial, 10) == 0 % for positive outcome (-0.5)
                    time = ((data(iTrial, 6) - T0time)/1000) + dummies + delayFeed; 
                    LossPalt = [LossPalt time];     
                    trialnr_LossPalt = [trialnr_LossPalt iTrial];
                end            
            end  
        end

        out{iRun}{1}.name  = 'GainRfactP';
        out{iRun}{1}.times = GainRfactP';
        out{iRun}{1}.trialnr = trialnr_GainRfactP;
        
        out{iRun}{2}.name  = 'GainNfactP';
        out{iRun}{2}.times = GainNfactP';
        out{iRun}{2}.trialnr = trialnr_GainNfactP;
        
        out{iRun}{3}.name  = 'LossNfactP';
        out{iRun}{3}.times = LossNfactP';
        out{iRun}{3}.trialnr = trialnr_LossNfactP;

        out{iRun}{4}.name  = 'LossPfactP';
        out{iRun}{4}.times = LossPfactP';
        out{iRun}{4}.trialnr = trialnr_LossPfactP;
                
        out{iRun}{5}.name  = 'GainRfactC';
        out{iRun}{5}.times = GainRfactC';
        out{iRun}{5}.trialnr = trialnr_GainRfactC;
        
        out{iRun}{6}.name  = 'GainNfactC';
        out{iRun}{6}.times = GainNfactC';
        out{iRun}{6}.trialnr = trialnr_GainNfactC;
        
        out{iRun}{7}.name  = 'LossNfactC';
        out{iRun}{7}.times = LossNfactC';
        out{iRun}{7}.trialnr = trialnr_LossNfactC;

        out{iRun}{8}.name  = 'LossPfactC';
        out{iRun}{8}.times = LossPfactC';
        out{iRun}{8}.trialnr = trialnr_LossPfactC;
        
        out{iRun}{9}.name  = 'GainRalt';
        out{iRun}{9}.times = GainRalt';
        out{iRun}{9}.trialnr = trialnr_GainRalt;
        
        out{iRun}{10}.name  = 'GainNalt';
        out{iRun}{10}.times = GainNalt';
        out{iRun}{10}.trialnr = trialnr_GainNalt;
        
        out{iRun}{11}.name  = 'LossNalt';
        out{iRun}{11}.times = LossNalt';
        out{iRun}{11}.trialnr = trialnr_LossNalt;

        out{iRun}{12}.name  = 'LossPalt';
        out{iRun}{12}.times = LossPalt';
        out{iRun}{12}.trialnr = trialnr_LossPalt;
        
         % CHECK: Are the onsets a column vector?                
        for j = 1:size(out{iRun},2)                  
            if size(out{iRun}{j}.times,2)>1                     
                error('The onsets are not a column vector!')                   
            end       
        end
    end
end
