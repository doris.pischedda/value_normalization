%% CONTRAST MANAGER: SETS CONTRASTS FOR 1ST LEVEL MODELS

% author: Doris Pischedda
%  start: 2018/01/24

clear all
clc

for sbj = 1:28;
    addpath('/analysis/doris/spm12/');
    dRoot = sprintf('/analysis/doris/ValNorm/02_Data/Sbj%02i/level1/outcome_all_HRF/univariate/', sbj);    

    matlabbatch{1}.spm.stats.con.spmmat = cellstr(spm_select('FPList', dRoot, '^SPM\.mat$'));
    matlabbatch{1}.spm.stats.con.consess{1}.tcon.name = 'Good > bad P';
    matlabbatch{1}.spm.stats.con.consess{1}.tcon.weights = [0.5 -0.5 0.5 -0.5 0 0 0 0 0 0 0 0];
    matlabbatch{1}.spm.stats.con.consess{1}.tcon.sessrep = 'repl';   
    matlabbatch{1}.spm.stats.con.consess{2}.tcon.name = 'Good > bad C';
    matlabbatch{1}.spm.stats.con.consess{2}.tcon.weights = [0 0 0 0 0.5 -0.5 0.5 -0.5 0 0 0 0];
    matlabbatch{1}.spm.stats.con.consess{2}.tcon.sessrep = 'repl';  
    matlabbatch{1}.spm.stats.con.consess{3}.tcon.name = 'Good > bad Alt';
    matlabbatch{1}.spm.stats.con.consess{3}.tcon.weights = [0 0 0 0 0 0 0 0 0.5 -0.5 0.5 -0.5];
    matlabbatch{1}.spm.stats.con.consess{3}.tcon.sessrep = 'repl';  
    matlabbatch{1}.spm.stats.con.delete = 1;
    spm_jobman('run',matlabbatch)
    
end