%% function matlabbatch = first_level_valNorm(sbj, cfg, whichtimings)

%  author: Doris Pischedda
%   start: 2017/10/27

% INPUT
%   sbj: integer
%   cfg.design.name: name of the design
% OPTIONAL
%   cfg.filter: filter for files to use (default: au*)
%   cfg.univariate_analysis: run analysis on normalized and smoothed data


function matlabbatch = first_level_valNorm(sbj, cfg, whichtimings)

%% Set paths

addpath('/analysis/doris/ValNorm/Analysis/')
addpath('/analysis/doris/spm12/');

%% CHANGE THIS FOR DIFFERENT CONDITIONS
% get timings
sessions = get_conditions(sbj, whichtimings) 
%% defaults

design_name = cfg.design.name;

%% prebatch processing

designID = [design_name '_HRF']

%% check number of

nrConditions = size(sessions.Sess(1).U,2);  % condition number in the design
nrSessions = size(sessions.Sess,2);         % number of sessions

%% SET DIRECTORIES

% Set data directory
basedir = sprintf('/analysis/doris/ValNorm/02_Data/Sbj%02i/', sbj);

% target dir
target_subdir = fullfile('level1', designID); 
if isfield(cfg, 'univariate_analysis') && cfg.univariate_analysis
    target_subdir = fullfile('level1', designID, 'univariate'); % for univariate analysis
end

% directory for output structure for this subject. Save with logfiles
target_dir = fullfile(basedir, target_subdir);

% file filter
if ~isfield(cfg, 'filter')   
    cfg.filter = 'auf*'; % not smoothed, not normalized
end

filter = cfg.filter;

for run_ind = 1:nrSessions
    run_subdir{run_ind} = sprintf('NIFTI/run%02i/%s', run_ind);
    
    run_permdir = fullfile(basedir, run_subdir{run_ind});
    if ~exist(run_permdir, 'dir')
        error('Directory %s does not exist', run_permdir);
    end
    
    remote_cfg.sources{run_ind} = fullfile(run_subdir{run_ind}, filter);
    remote_cfg.sources{run_ind+nrSessions} = fullfile(run_subdir{run_ind}, 'rp*.*');

    run_dir{run_ind} = run_permdir;  

end

%% check for beta warnings

% check number of entries per beta
for cond_ind = 1:nrConditions
    for sess_ind = 1:nrSessions
        if length(sessions.Sess(sess_ind).U(cond_ind).ons) < 3
            if ~exist('beta_warnings', 'var')
                beta_warnings = {}
            end
            warn_str = sprintf('Sbj %i, Cond %i, Sess %i: Only found %i images for one beta', sbj, ...
                cond_ind, sess_ind, length(sessions.Sess(sess_ind).U(cond_ind).ons));
            warning(warn_str)
            beta_warnings{end+1} = warn_str;
        end
    end
end

% save warnings
if exist('beta_warnings', 'var')
    betawarnings_dir = fullfile(basedir, 'beta_warnings')
    mkdir(betawarnings_dir)
    betawarnings_file = fullfile(betawarnings_dir, ['betawarnings_' design_name]);
    disp(' ')
    disp('!!!!!')
    disp(['Saving betawarnings to ' betawarnings_file])
    disp('!!!!!')
    disp(' ')
    save(betawarnings_file, 'beta_warnings')
end

%% create target directory

mkdir(target_dir)

%% start spm fmri

spm fmri

%% SPM BATCH: model setting

display('Setting up model generation parameters')
% directory for output strucutre
matlabbatch{1}.spm.stats.fmri_spec.dir = {target_dir};
matlabbatch{1}.spm.stats.fmri_spec.timing.units = 'secs';
matlabbatch{1}.spm.stats.fmri_spec.timing.RT = 2.2;
matlabbatch{1}.spm.stats.fmri_spec.timing.fmri_t = 16;
matlabbatch{1}.spm.stats.fmri_spec.timing.fmri_t0 = 8;

for iSes = 1:nrSessions

    matlabbatch{1}.spm.stats.fmri_spec.sess(iSes).scans = cellstr(spm_select('FPList',run_dir{iSes},['^' filter '.*\.nii$']));

    % CHECK: no files
    if isempty(matlabbatch{1}.spm.stats.fmri_spec.sess(iSes).scans{1})
        error('Files are empty, is that correct?')
    end

    % CHECK: verify that scans are a column matrix
    if size(matlabbatch{1}.spm.stats.fmri_spec.sess(iSes).scans, 2) > 1
        error('Files are no column matrix')
    end

    for iCond = 1:nrConditions
        matlabbatch{1}.spm.stats.fmri_spec.sess(iSes).cond(iCond).name = sessions.Sess(iSes).U(iCond).name{1};
        matlabbatch{1}.spm.stats.fmri_spec.sess(iSes).cond(iCond).onset = sessions.Sess(iSes).U(iCond).ons;       
        matlabbatch{1}.spm.stats.fmri_spec.sess(iSes).cond(iCond).duration = 0; 
        matlabbatch{1}.spm.stats.fmri_spec.sess(iSes).cond(iCond).tmod = 0;                
        matlabbatch{1}.spm.stats.fmri_spec.sess(iSes).cond(iCond).pmod = struct('name', {}, 'param', {}, 'poly', {});            
       

        % CHECK: verify that condition onset and duration are column vectors
        if size(matlabbatch{1}.spm.stats.fmri_spec.sess(iSes).cond(iCond).onset, 2) > 1
            error('Onsets are not column vectors')
        end
        
        if size(matlabbatch{1}.spm.stats.fmri_spec.sess(iSes).cond(iCond).duration, 2) > 1
            error('Durations are not column vectors')
        end
        
    end

    matlabbatch{1}.spm.stats.fmri_spec.sess(iSes).multi = {''};
    matlabbatch{1}.spm.stats.fmri_spec.sess(iSes).regress = struct('name', {}, 'val', {});  
    matlabbatch{1}.spm.stats.fmri_spec.sess(iSes).multi_reg = {''};
    matlabbatch{1}.spm.stats.fmri_spec.sess(iSes).hpf = 128;

end

matlabbatch{1}.spm.stats.fmri_spec.fact = struct('name', {}, 'levels', {});
matlabbatch{1}.spm.stats.fmri_spec.volt = 1;
matlabbatch{1}.spm.stats.fmri_spec.global = 'None';
matlabbatch{1}.spm.stats.fmri_spec.mask = {''};
matlabbatch{1}.spm.stats.fmri_spec.cvi = 'AR(1)';
matlabbatch{1}.spm.stats.fmri_spec.bases.hrf.derivs = [0 0];

%% SPM BATCH: model estimation

display('Adding Model estimation step')
matlabbatch{2}.spm.stats.fmri_est.spmmat = {[target_dir '/SPM.mat']};
matlabbatch{2}.spm.stats.fmri_est.method.Classical = 1;

%% run the batch

if runn(1)
    start_matlabbatch(matlabbatch, '1st_level')
end

if exist('beta_warnings', 'var')
    warning(['Betawarnings exist! See ' betawarnings_file])
end

disp('Batch finished')

end