%% Returns a structure with timings for all conditions defined in the get_timings_XXX.m script

%  author: Doris Pischedda
%   start: 2017/10/27

function sessions = get_conditions(sbj,whichtimings)

ver = 'doris, 2017/10/27';

ffname = str2func(['get_timings_' whichtimings]); % create the function handle from a string

% get data
out = ffname(sbj);                     

% convert data to SPM-design structure
nrSessions = length(out);

% corresponding timing to put into sess variable (for SPM.Sess)
Sess = struct;
for sess_ind = 1:nrSessions
    Sess(sess_ind).info.function = ['Generated by ' mfilename ' ver ' ver ' @' datestr(now)];
    Sess(sess_ind).info.subjnr = sbj;
    Sess(sess_ind).info.time_generated = datestr(now);
    
    for i = 1:size(out{sess_ind},2)
        Sess(sess_ind).U(i).name{1} = out{sess_ind}{i}.name;
        Sess(sess_ind).U(i).ons = out{sess_ind}{i}.times;
        Sess(sess_ind).U(i).dur = 0;
        Sess(sess_ind).U(i).P(1).name = 'none';
        Sess(sess_ind).U(i).units = 'secs';
    end
end

% save for returning
sessions.Sess = Sess;
sessions.designname = ['cue_onsets_' whichtimings];
