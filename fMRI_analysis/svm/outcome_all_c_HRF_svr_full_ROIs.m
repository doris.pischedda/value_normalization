%% MAIN: SUPPORT VECTOR REGRESSION FOR VALUE OF FACTUAL OUTCOME (GAIN&REW, GAIN&NULL, LOSS&NULL, LOSS&PUN) ACCORDING TO FULL ADAPTATION IN TRIALS WITH COMPLETE FEEDBACK 

% author: Doris Pischedda
%  start: 2019/12/10


function outcome_all_c_HRF_svr_full_ROIs(jobnr)

% Set paths

addpath('/analysis/doris/ValNorm/Analysis/svm')
addpath(genpath('/analysis/doris/ValNorm/Analysis/decoding_toolbox_v3.97'))
addpath('/analysis/doris/spm12/'); % Add spm


%% CONFIG: specify this for proper folder names

whichmeasure = 'zcorr';
whichmodel = 'outcome_all_HRF';

%% CONFIG: specify which conditions and bins and types you want to compute

subjectList = [1:28];  % for which subjects
sbj = subjectList(jobnr);

%% START DECODING

allclasses = {'GainRfactC' 
              'GainNfactC' 
              'LossNfactC' 
              'LossPfactC'};

designname = [ mfilename '_ncls' num2str(length(allclasses))];
radius = 4;  % searchlight radius in voxels

labelnames = {};
for ind = 1:length(allclasses)
    labelnames{ind} = [allclasses{ind}];
end

labels = [2 -2 2 -2];

resultsRoot = ['/analysis/doris/ValNorm/design/' designname '/'];

filePrefix = ['svm_' designname '_r' num2str(radius) '_'];  % for renaming
if iscell(whichmeasure)
    fileName = [ filePrefix 'res_' whichmeasure{1} ];
else
    fileName = [ filePrefix 'res_' whichmeasure ];
end

 % For ROI-based decoding
    subject_ROI_dir = sprintf('/analysis/doris/ValNorm/02_Data/Sbj%02i/subjectspace_ROIs', sbj)
    ROIfiles = {fullfile(subject_ROI_dir, 'iw1.nii');
    fullfile(subject_ROI_dir, 'iw2.nii');
    fullfile(subject_ROI_dir, 'iw11.nii');
    fullfile(subject_ROI_dir, 'iw12.nii');
    fullfile(subject_ROI_dir, 'iw13.nii');
    fullfile(subject_ROI_dir, 'iw14.nii');
    fullfile(subject_ROI_dir, 'iw41.nii');
    fullfile(subject_ROI_dir, 'iw42.nii');
    fullfile(subject_ROI_dir, 'iw47.nii');
    fullfile(subject_ROI_dir, 'iw48.nii');
    fullfile(subject_ROI_dir, 'iw49.nii');
    fullfile(subject_ROI_dir, 'iw50.nii');
    fullfile(subject_ROI_dir, 'iw179.nii');
    fullfile(subject_ROI_dir, 'iw180.nii');
    fullfile(subject_ROI_dir, 'iw183.nii');
    fullfile(subject_ROI_dir, 'iw184.nii');
    fullfile(subject_ROI_dir, 'iw187.nii');
    fullfile(subject_ROI_dir, 'iw188.nii');
    }

% DECODING
decode_cv_svr_ROI(labelnames, labels, resultsRoot, subjectList(jobnr), whichmeasure, whichmodel, radius, filePrefix, ROIfiles)

display(['Finished: decoding subject ' num2str(jobnr)])

end