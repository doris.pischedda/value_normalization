%% MAIN: DECODING COUNTERFACTUAL OUTCOME (POSITIVE VS. NEGATIVE), BINARY CLASSIFICATION FOR GAIN&REW, GAIN&NULL, LOSS&NULL, LOSS&PUN IN TRIALS WITH COMPLETE INFORMATION

% author: Doris Pischedda
%  start: 2019/12/09


function outcome_all_counter_HRF_ROIs(jobnr)

% Set paths

addpath('/analysis/doris/ValNorm/Analysis/svm')
addpath(genpath('/analysis/doris/ValNorm/Analysis/decoding_toolbox_v3.97'))
addpath('/analysis/doris/spm12/'); % Add spm

%% CONFIG: specify this for proper folder names

whichmeasure = 'accuracy_minus_chance';
whichmodel = 'outcome_all_HRF';

%% CONFIG: specify which conditions and bins and types you want to compute

subjectList = [1:28];  % for which subjects
sbj = subjectList(jobnr);

%% START DECODING

for whichcond = 1:4

    if whichcond == 1
        trainA = 'GainRalt';
        trainB = 'GainNalt';
    elseif whichcond == 2
        trainA = 'GainRalt';
        trainB = 'LossPalt';
    elseif whichcond == 3
        trainA = 'LossNalt';
        trainB = 'GainNalt';
    elseif whichcond == 4
        trainA = 'LossNalt';
        trainB = 'LossPalt';
    end

    designname = [ mfilename '_train_' trainA '_' trainB ];
    radius = 4;  % searchlight radius in voxels

    labelnames = {trainA trainB}
    labels = [1 -1]
    
    resultsRoot = ['/analysis/doris/ValNorm/design/' designname '/'];
    filePrefix = ['svm_' designname '_r' num2str(radius) '_'];  % for renaming
    fileName = [ filePrefix 'res_' whichmeasure ];
    
    subject_ROI_dir = sprintf('/analysis/doris/ValNorm/02_Data/Sbj%02i/subjectspace_ROIs', sbj)
    ROIfiles = {fullfile(subject_ROI_dir, 'iw1.nii');
    fullfile(subject_ROI_dir, 'iw2.nii');
    fullfile(subject_ROI_dir, 'iw11.nii');
    fullfile(subject_ROI_dir, 'iw12.nii');
    fullfile(subject_ROI_dir, 'iw13.nii');
    fullfile(subject_ROI_dir, 'iw14.nii');
    fullfile(subject_ROI_dir, 'iw41.nii');
    fullfile(subject_ROI_dir, 'iw42.nii');
    fullfile(subject_ROI_dir, 'iw47.nii');
    fullfile(subject_ROI_dir, 'iw48.nii');
    fullfile(subject_ROI_dir, 'iw49.nii');
    fullfile(subject_ROI_dir, 'iw50.nii');
    fullfile(subject_ROI_dir, 'iw179.nii');
    fullfile(subject_ROI_dir, 'iw180.nii');
    fullfile(subject_ROI_dir, 'iw183.nii');
    fullfile(subject_ROI_dir, 'iw184.nii');
    fullfile(subject_ROI_dir, 'iw187.nii');
    fullfile(subject_ROI_dir, 'iw188.nii');
    }

    % DECODING
    decode_cv_ROI(labelnames, labels, resultsRoot, subjectList(jobnr), whichmeasure, whichmodel, radius, filePrefix, ROIfiles)

    display(['Finished: decoding condition ' num2str(whichcond)])
end

end