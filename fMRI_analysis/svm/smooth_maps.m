%% SMOOTHER: SMOOTH ACCURACY MAPS 

% author: Doris Pischedda
%  start: 2017/12/13

clear all
clc
    
addpath('/analysis/doris/spm12/'); % Add spm

rootDir = '/analysis/doris/ValNorm/design/outcome_all_counter_HRF_svr_partial_ncls4/'; % change folder name        

matlabbatch{1}.spm.spatial.smooth.data = cellstr(spm_select('FPList', rootDir, '^wsub.*')); % selects normalized maps
matlabbatch{1}.spm.spatial.smooth.fwhm = [6 6 6];
matlabbatch{1}.spm.spatial.smooth.dtype = 0;
matlabbatch{1}.spm.spatial.smooth.im = 0;
matlabbatch{1}.spm.spatial.smooth.prefix = 's6';
spm_jobman('run',matlabbatch)