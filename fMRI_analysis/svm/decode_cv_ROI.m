%% DECODING WRAPPER, CROSS VALIDATION

% author: Doris Pischedda
%  start: 2019/12/06


function decode_cv_ROI(labelnames, labels, resultsRoot, sbj, whichmeasure, whichmodel, radius, filePrefix, ROIfiles)

mkdir(resultsRoot) % make result folder

cfg = decoding_defaults; % set defaults

cfg.analysis = 'ROI';


cfg.results.dir = fullfile(resultsRoot, 'ROI');  % where to write results
if iscell(whichmeasure)
    cfg.results.output = whichmeasure;
else
    cfg.results.output = { whichmeasure };
end
cfg.results.write = 1; % write results
cfg.results.filestart = sprintf(['sub%02i_' filePrefix 'res'], sbj); % set the subject specific filestart here already (instead of copying and renaming in the end)
cfg.results.setwise = 0;
cfg.results.overwrite = 1; % overwrite existing results

beta_dir = sprintf(['/analysis/doris/ValNorm/02_Data/Sbj%02i/level1/' whichmodel], sbj);
cfg.files.mask = ROIfiles; 

regressor_names = design_from_spm(beta_dir);

cfg = decoding_describe_data(cfg, labelnames, labels, regressor_names, beta_dir);

cfg.design = make_design_cv(cfg);
cfg.design.set = 1:size(cfg.design.train, 2);

try
    plot_design(cfg);
end
    
cfg.verbose = 2;

cfg.searchlight.unit = 'voxels';
cfg.searchlight.radius = radius;
cfg.searchlight.wrap_control = 1;

cfg.software = 'SPM12';

% Run decoding
% cfg.testmode = 1
[results, cfg] = decoding(cfg);

end