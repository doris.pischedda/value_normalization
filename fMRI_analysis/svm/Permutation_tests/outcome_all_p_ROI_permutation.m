%% Create a permutation design for analysis of factual outcome in trial with partial feedback

% author: Doris Pischedda
%  start: 2019/12/16
 

function outcome_all_p_ROI_permutation(sbj)

% Check that SPM and TDT are available on the path
if isempty(which('SPM')), error('Please add SPM to the path and restart'), end
if isempty(which('decoding_defaults')), error('Please add TDT to the path and restart'), end
decoding_defaults; % add all important directories to the path

% All possible binary classifications
trainA{1} = 'GainRfactP_GainNfactP';
trainA{2} = 'GainRfactP_LossPfactP';
trainA{3} = 'LossNfactP_GainNfactP';
trainA{4} = 'LossNfactP_LossPfactP';

%% Load cfg from the analysis that has been performed
% load file for participant

basedir = '/analysis/doris/ValNorm/design/outcome_all_p_HRF_ROIs_train_';

for i = 1:length(trainA)
    cfg_file = sprintf([basedir trainA{i} '/ROI/sub%02i_svm_outcome_all_p_HRF_ROIs_train_' trainA{i} '_r4_res_cfg.mat'], sbj);
    display(['Loading ' cfg_file]);
    load(cfg_file, 'cfg');
    org_cfg = cfg; % keeping the unpermuted cfg to copy parameters below
    %% Create cfg with permuted sets
    cfg = org_cfg; % initialize new cfg like the original

    cfg = rmfield(cfg,'design'); 
    cfg.design.function = org_cfg.design.function;

    cfg.results = rmfield(cfg.results, 'resultsname'); % the name should be generated later
    cfg.results.dir = fullfile(cfg.results.dir, 'perm', trainA{i}); % change directory
    cfg.results.overwrite = 1; % should not overwrite results (change if you whish to do so)
    cfg.results.setwise = 1; % set to 1 
    
    if strcmp(cfg.analysis, 'searchlight')
        cfg.plot_selected_voxels = 1000; % show SL every 1000' steps
    end

    n_perms = 8; % number of label permutations

    combine = 0;   % change to 1 to calculate the design only once
    designs = make_design_permutation(cfg,n_perms,combine);

    %% Run all permutations in a loop

    cfg.fighandles.plot_design = figure(); % open one figure for all designs
    passed_data = []; % avoid loading the same data multiple times by looping it
    for i_perm = 1:n_perms
        dispv(1, 'Permutation %i/%i', i_perm, n_perms)

        cfg.design = designs{i_perm};
        cfg.results.filestart = ['perm' sprintf('%04d',i_perm) sprintf('_sbj%02d',sbj)];

        set(cfg.fighandles.plot_design, 'name', sprintf('Permutation %i/%i', i_perm, n_perms)); % to know where we are
        if ~strcmp(cfg.analysis, 'searchlight') && i_perm > 1
            cfg.plot_selected_voxels = 0; % switch off after the first time, drawing takes some time
        end

        % do the decoding for this permutation
        [results, final_cfg, passed_data] = decoding(cfg, passed_data); % run permutation

        % rename  design figures to start with the current permutation number
        designfiles = dir(fullfile(cfg.results.dir, 'design.*'));
        for design_ind = 1:length(designfiles)
            movefile(fullfile(cfg.results.dir, designfiles(design_ind).name), ...
                        fullfile(cfg.results.dir, [cfg.results.filestart '_' designfiles(design_ind).name]));
        end
    end
end

