%% DECODING WRAPPER, CROSS VALIDATION FOR SUPPORT VECTOR REGRESSION

% author: Doris Pischedda
%  start: 2010/12/10


function decode_cv_svr_ROI(labelnames, labels, resultsRoot, sbj, whichmeasure, whichmodel, radius, filePrefix, ROIfiles)

mkdir(resultsRoot) % make result folder

cfg = decoding_defaults; % set TDT defaults

cfg.analysis = 'ROI';


cfg.results.dir = fullfile(resultsRoot, 'ROI'); % folder where to write results

if iscell(whichmeasure)
    cfg.results.output = whichmeasure;
else
    cfg.results.output = { whichmeasure };
end
cfg.results.write = 1; % writes results
cfg.results.filestart = sprintf(['sub%02i_' filePrefix 'res'], sbj); % set the subject specific filestart
cfg.results.setwise = 0;
cfg.results.overwrite = 1; % overwrites existing results

beta_dir = sprintf(['/analysis/doris/ValNorm/02_Data/Sbj%02i/level1/' whichmodel], sbj);

cfg.decoding.method = 'regression'; % does regression instead
cfg.files.mask = ROIfiles; 

regressor_names = design_from_spm(beta_dir);

cfg = decoding_describe_data(cfg, labelnames, labels, regressor_names, beta_dir);

cfg.design = make_design_cv(cfg); % cross-validated design
cfg.design.set = 1:size(cfg.design.train, 2);
cfg.design.unbalanced_data = 'ok'; % no error with unbalanced data

try
    plot_design(cfg);
end
    
cfg.verbose = 2;

cfg.searchlight.unit = 'voxels';
cfg.searchlight.radius = radius;
cfg.searchlight.wrap_control = 1;

cfg.software = 'SPM12'; % set SPM version

% Run decoding
[results, cfg] = decoding(cfg);

end