% Calculate the mean contrast value for counterfactual outcome in trials with complete information in the ROIs
% in the list for the specified sbjs

% author: Doris Pischedda
%  start: 2019/11/19


clear all

subjectList = [1:28];
mean_activation_roi = zeros(length(subjectList), 1); 

%% Load ROI data
% ROI file list
ROI_files = {  
    'ns1.nii';
    'ns2.nii';
    'ns11.nii';
    'ns12.nii';
    'ns13.nii';
    'ns14.nii';
    'ns41.nii';
    'ns42.nii';
    'ns47.nii';
    'ns48.nii';
    'ns49.nii';
    'ns50.nii';     
    'ns179.nii';
    'ns180.nii';  
    'ns183.nii';   
    'ns184.nii';   
    'ns187.nii';   
    'ns188.nii';         
    }; 
        
for ROI_file_ind = 1:length(ROI_files)

    mean_activation_roi = [];
    
        
    for i = 1:length(subjectList)
        sbj = subjectList(i); 
        sbjID(i, 1) = sbj;

        display(sprintf('ROI %i/%i, sbj %i/%i', ROI_file_ind, length(ROI_files), i, length(subjectList)))

        %% Load SL accuracy data

        folder = sprintf('/analysis/doris/ValNorm/02_Data/Sbj%02i/level1/outcome_all_HRF/univariate', sbj);
%         file{1} = 'beta_0009.nii';
%         file{2} = 'beta_0021.nii';
%         file{3} = 'beta_0033.nii';
%         file{4} = 'beta_0045.nii';
%         file{1} = 'beta_0010.nii';
%         file{2} = 'beta_0022.nii';
%         file{3} = 'beta_0034.nii';
%         file{4} = 'beta_0046.nii';
%         file{1} = 'beta_0011.nii';
%         file{2} = 'beta_0023.nii';
%         file{3} = 'beta_0035.nii';
%         file{4} = 'beta_0047.nii';
        file{1} = 'beta_0012.nii';
        file{2} = 'beta_0024.nii';
        file{3} = 'beta_0036.nii';
        file{4} = 'beta_0048.nii';

        for ii = 1:length(file)
            hdr = spm_vol(fullfile(folder, file{ii}));
            vol = spm_read_vols(hdr);

            roi_folder = sprintf('/analysis/doris/ValNorm/02_Data/Sbj%02i/subjectspace_ROIs', sbj);

            roi_file = ROI_files{ROI_file_ind};
            hdr_r = spm_vol(fullfile(roi_folder, roi_file));
            vol_roi = spm_read_vols(hdr_r);

            % Checks
            if isequal(unique(vol_roi), [0 1])
                unique(vol_roi)
                error('ROI contains values other than 0 and 1 (maybe only 0?)')
            end

            %% Calculate mean
            mean_roi_beta(ii) = nanmean(vol(vol_roi>0)); 
        end           
        mean_roi_act(i) = nanmean(mean_roi_beta(:)); 

    end
        
display('Adding subj ID as last column')
mean_activation_roi = [mean_roi_act', sbjID];

ROI{ROI_file_ind} = mean_activation_roi;

end

% Save to csv file
display('Save to csv file')
% dlmwrite('counterfactual_all_univ_betas_GR_ROIs.csv', ROI, 'Delimiter', ';')
% dlmwrite('counterfactual_all_univ_betas_GN_ROIs.csv', ROI, 'Delimiter', ';')
% dlmwrite('counterfactual_all_univ_betas_LN_ROIs.csv', ROI, 'Delimiter', ';')
dlmwrite('counterfactual_all_univ_betas_LP_ROIs.csv', ROI, 'Delimiter', ';')
    
display('All done')