%% Run inverse normalization


% Settings
overwrite_mask = 0; % By default (0), no file will be created if the target file already exist.
                    % If you want to re-create the ROIs for whatever reason, set overwrite_mask = 1;

% set data directory
basedir = '/analysis/doris/ValNorm/02_Data/';

% add ROI files here  
ROI_files = {  
    '/analysis/doris/ValNorm/Analysis/ROI/ROI_decoding/ROIs/1.nii';
    '/analysis/doris/ValNorm/Analysis/ROI/ROI_decoding/ROIs/2.nii';
    '/analysis/doris/ValNorm/Analysis/ROI/ROI_decoding/ROIs/11.nii';
    '/analysis/doris/ValNorm/Analysis/ROI/ROI_decoding/ROIs/12.nii';
    '/analysis/doris/ValNorm/Analysis/ROI/ROI_decoding/ROIs/13.nii';
    '/analysis/doris/ValNorm/Analysis/ROI/ROI_decoding/ROIs/14.nii';
    '/analysis/doris/ValNorm/Analysis/ROI/ROI_decoding/ROIs/41.nii';
    '/analysis/doris/ValNorm/Analysis/ROI/ROI_decoding/ROIs/42.nii';
    '/analysis/doris/ValNorm/Analysis/ROI/ROI_decoding/ROIs/47.nii';
    '/analysis/doris/ValNorm/Analysis/ROI/ROI_decoding/ROIs/48.nii';
    '/analysis/doris/ValNorm/Analysis/ROI/ROI_decoding/ROIs/49.nii';
    '/analysis/doris/ValNorm/Analysis/ROI/ROI_decoding/ROIs/50.nii';    
    '/analysis/doris/ValNorm/Analysis/ROI/ROI_decoding/ROIs/179.nii';
    '/analysis/doris/ValNorm/Analysis/ROI/ROI_decoding/ROIs/180.nii';  
    '/analysis/doris/ValNorm/Analysis/ROI/ROI_decoding/ROIs/183.nii';   
    '/analysis/doris/ValNorm/Analysis/ROI/ROI_decoding/ROIs/184.nii';   
    '/analysis/doris/ValNorm/Analysis/ROI/ROI_decoding/ROIs/187.nii';   
    '/analysis/doris/ValNorm/Analysis/ROI/ROI_decoding/ROIs/188.nii';    
    };   


% add sbjs here
subjects = [1:28]; 

%% Add paths

% add current file
filedir = fileparts(which('inverse_normalize_ROIs_valNorm'));
if isempty(filedir)
    error('Could not find inverse_normalize_ROIs_valNorm.m please check it is in path')
else
    addpath(fileparts(which('inverse_normalize_ROIs_valNorm')));
end

% add spm12
addpath('/analysis/doris/spm12/')

%% Loop over sbjs

for sbj_ind = 1:length(subjects)
    sbj = subjects(sbj_ind);
    
    % sbj subdirecory
    base_subdir = fullfile(basedir, sprintf('Sbj%02i', sbj));

    % where to put the inverse normalized rois
    target_dir = fullfile(base_subdir, 'subjectspace_ROIs');
    if ~exist(target_dir, 'dir')
        mkdir(target_dir)
        if ~exist(target_dir, 'dir')
            error('Could not create directory for target rois, aborting')
        end
    end
    display(['Saving inverse normalized ROIs to: ' target_dir])
     
    % get subject to MNI normalization file
    struct_dir = fullfile(base_subdir, 'NIFTI', 'T1');
    run01_dir = fullfile(base_subdir, 'NIFTI', 'run01');
    searchstring = fullfile(struct_dir, 'iy_*.nii');
    w_file = dir(searchstring); 
    if length(w_file) ~= 1
        error('Found %i occurences of normalization-searchstring for matfile for subject %i in %s', length(w_file), sbj, searchstring)
    else
        w_file = fullfile(struct_dir, w_file.name);
        display(['Normalization file: ' w_file])
    end

    % get any arf subject sourcespace file (for dimensions)
    searchstring = fullfile(run01_dir,'auf*.nii');
    subject_sourcespace_image = dir(searchstring); 
    if isempty(subject_sourcespace_image)
        error('No arf sourcespace file found for subject %i in %s', sbj, searchstring)
    else
        subject_sourcespace_image = fullfile(run01_dir, subject_sourcespace_image(1).name);
        display(['Sourcespace example image: ' subject_sourcespace_image])
    end
    
    
    for ROI_file_ind = 1:length(ROI_files)
        img = ROI_files{ROI_file_ind}; % the ROI to inverse normlize
        imaskname = inverse_normalize_ROIs_valNorm(img,w_file,target_dir,subject_sourcespace_image, overwrite_mask);
    end
end