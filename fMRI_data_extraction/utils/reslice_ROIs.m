%% Normalize ROIs

% author: Doris Pischedda
%  start: 2018/02/27

clear all
addpath('/analysis/doris/spm12/'); % add SPM folder to path

for sbj = 1:28
    
    basedir = sprintf('/analysis/doris/ValNorm/02_Data/Sbj%02i/subjectspace_ROIs/', sbj); % folder with ROI files
    resdir = sprintf('/analysis/doris/ValNorm/design/outcome_all_p_HRF_ROIs_train_GainRfactP_GainNfactCP/ROI/'); %  folder with reference image
    filter = sprintf('swsub%02_svm', sbj);
    
    spm_jobman('initcfg');
    
    matlabbatch{1}.spm.spatial.coreg.write.ref = cellstr(spm_select('FPList', resdir, ['^' filter '.*\.nii$'])) ; % selects reference image  
    matlabbatch{1}.spm.spatial.coreg.write.source = cellstr(spm_select('FPList', basedir, '.*\.nii$')) ; % selects images to reslice  
    matlabbatch{1}.spm.spatial.coreg.write.roptions.interp = 0;
    matlabbatch{1}.spm.spatial.coreg.write.roptions.wrap = [0 0 0];
    matlabbatch{1}.spm.spatial.coreg.write.roptions.mask = 0;
    matlabbatch{1}.spm.spatial.coreg.write.roptions.prefix = 'ns'; % prefix for resliced images
    
    spm_jobman('run', matlabbatch);
end
    