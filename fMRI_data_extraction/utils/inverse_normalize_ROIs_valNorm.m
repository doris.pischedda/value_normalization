% function imaskname_all = inverse_normalize_ROIs_tm03(img,w_mat,target_dir,subject_sourcespace_image, overwrite_mask)
%
% IN
% overwrite_mask: set to 1 if you dont want to load the file if it exists
%
% 2nd LEVEL MASK file (used for dimensions) is hardcoded in this file below.
%
% Called back_w.m in TM_logic
%
% INPUT
%   img: List of images to inverse normalize as cellstr (or single string 
%       with single image)
%   w_mat: full path to original normalization *_sn.mat that was used as
%       forward normalization
%   target_dir: target directory. Normalized files will be copied and 
%       inverse normlized iw*.* files will be created here. Directory will 
%       be created, if it does not exist.
%   subject_sourcespace_image: unnormlized example image (i.e. from the
%       original source space). Take e.g. a 'arf*' file from the
%       preprocessing.
%   overwrite: if 0, files will not be overwritten. Default: 0
%
% OUT
%   imaskname_all: Full path to inverse normalized files as cellstr. All
%       files are returned here, also those that already existed before


function imaskname_all = inverse_normalize_ROIs_valNorm(img,w_file,target_dir,subject_sourcespace_image, overwrite_mask)

if ~exist('overwrite_mask', 'var')
    overwrite_mask = 0;
end

% make a cellstring if only a string

if ~iscellstr(img)
    img = cellstr(img);
end


%% Inverse calculation and image creation

matlabbatch{1}.spm.util.defs.comp{1}.inv.comp{1}.def = {w_file};
matlabbatch{1}.spm.util.defs.comp{1}.inv.space = {subject_sourcespace_image};
matlabbatch{1}.spm.util.defs.out{1}.push.fnames = img;
matlabbatch{1}.spm.util.defs.out{1}.push.weight = {''};
matlabbatch{1}.spm.util.defs.out{1}.push.savedir.saveusr = {target_dir};
matlabbatch{1}.spm.util.defs.out{1}.push.fov.file = {subject_sourcespace_image};
matlabbatch{1}.spm.util.defs.out{1}.push.preserve = 0;
matlabbatch{1}.spm.util.defs.out{1}.push.fwhm = [0 0 0];
matlabbatch{1}.spm.util.defs.out{1}.push.prefix = 'w';


%% Check if final image exists and, if so, return it without doing anything

rem_ind = [];
for img_ind = 1:length(img)
    [odir, curr_filename] = fileparts(img{img_ind});
    imaskname{img_ind} = fullfile(target_dir, ['iw' curr_filename]); % target image name
    
    if ~overwrite_mask && (exist([imaskname{img_ind} '.hdr'], 'file') || exist([imaskname{img_ind} '.nii'], 'file'))
        % remeber to take out below
        rem_ind(end+1) = img_ind;
    end
end


imaskname_all = imaskname; % keep for return

% remove files that already exist
imaskname(rem_ind) = [];

if isempty(imaskname)
    display('No imagefiles left (after checking if target files exist, returning')
    return
else

%% Do everything
    if ~exist(target_dir, 'dir')
        mkdir(targetdir)
        if ~exist(target_dir, 'dir')
            error('Could not create target dir %s', targetdir)
        end
    end


    for img_ind = 1:length(img)
        display(['Copying ' img{img_ind} ' to ' target_dir])
        copyfile(img{img_ind},target_dir);
        % also copy hdr if file is .img (and not .nii)
        if strcmp(img{img_ind}(end-2:end), 'img')
            display(['Copying ' strrep(img{img_ind}, '.img', '.hdr') ' to ' target_dir])
            copyfile(strrep(img{img_ind}, '.img', '.hdr'),target_dir);
        end
    end

    display('Running prepared batch for inverse normalization')
    spm_jobman('run',matlabbatch);
    
    % rename files to put an i in front of everything
    for img_ind = 1:length(img)
        display('Adding leading iw to mark inverse normalisation')
        [odir, curr_filename] = fileparts(img{img_ind});
        try
            movefile(fullfile(target_dir, ['w' curr_filename '.img']), fullfile(target_dir, ['iw' curr_filename '.img'])); % target image name
            movefile(fullfile(target_dir, ['w' curr_filename '.hdr']), fullfile(target_dir, ['iw' curr_filename '.hdr'])); % target image name
        catch
            movefile(fullfile(target_dir, ['w' curr_filename '.nii']), fullfile(target_dir, ['iw' curr_filename '.nii'])); % target image name
        end
    end
    
end