%% Create single files with effects

clear all

% Import csv data
partial_uni_GR = readtable('partial_all_univ_betas_GR_ROIs.csv');
partial_uni_GN = readtable('partial_all_univ_betas_GN_ROIs.csv');
partial_uni_LN = readtable('partial_all_univ_betas_LN_ROIs.csv');
partial_uni_LP = readtable('partial_all_univ_betas_LP_ROIs.csv');
complete_uni_GR = readtable('complete_all_univ_betas_GR_ROIs.csv');
complete_uni_GN = readtable('complete_all_univ_betas_GN_ROIs.csv');
complete_uni_LN = readtable('complete_all_univ_betas_LN_ROIs.csv');
complete_uni_LP = readtable('complete_all_univ_betas_LP_ROIs.csv');
counterfactual_uni_GR = readtable('counterfactual_all_univ_betas_GR_ROIs.csv');
counterfactual_uni_GN = readtable('counterfactual_all_univ_betas_GN_ROIs.csv');
counterfactual_uni_LN = readtable('counterfactual_all_univ_betas_LN_ROIs.csv');
counterfactual_uni_LP = readtable('counterfactual_all_univ_betas_LP_ROIs.csv');

% Convert to matrix and get rid of sbj IDs
partial_GR = partial_uni_GR{:,1:2:36};
partial_GN = partial_uni_GN{:,1:2:36};
partial_LN = partial_uni_LN{:,1:2:36};
partial_LP = partial_uni_LP{:,1:2:36};
complete_GR = complete_uni_GR{:,1:2:36};
complete_GN = complete_uni_GN{:,1:2:36};
complete_LN = complete_uni_LN{:,1:2:36};
complete_LP = complete_uni_LP{:,1:2:36};
counterfactual_GR = counterfactual_uni_GR{:,1:2:36};
counterfactual_GN = counterfactual_uni_GN{:,1:2:36};
counterfactual_LN = counterfactual_uni_LN{:,1:2:36};
counterfactual_LP = counterfactual_uni_LP{:,1:2:36};

% Combine betas in a single file
partial_uni_all = [partial_GR partial_GN partial_LN partial_LP];
complete_uni_all = [complete_GR complete_GN complete_LN complete_LP];
counterfactual_uni_all = [counterfactual_GR counterfactual_GN counterfactual_LN counterfactual_LP];

% Calculate contrast effects
numROI = 18;

for iRoi = 1:numROI
    colGR = numROI*0 + iRoi; 
    colGN = numROI*1 + iRoi;
    colLN = numROI*2 + iRoi; 
    colLP = numROI*3 + iRoi;
    partial(:,iRoi) = partial_uni_all(:, colGR) - partial_uni_all(:, colGN) + partial_uni_all(:, colLN) - partial_uni_all(:,colLP);
    complete(:,iRoi) = complete_uni_all(:, colGR) - complete_uni_all(:, colGN) + complete_uni_all(:, colLN) - complete_uni_all(:,colLP);
    counterfactual(:,iRoi) = counterfactual_uni_all(:, colGR) - counterfactual_uni_all(:, colGN) + counterfactual_uni_all(:, colLN) - counterfactual_uni_all(:,colLP);

end

% Normalize data
partial_uni_Zscore = zscore(partial,0,2);
complete_uni_Zscore = zscore(complete,0,2);
counterfactual_uni_Zscore = zscore(counterfactual,0,2);


% Save to csv files
dlmwrite('partial_factual_all_univ_betas_ROIs.csv', partial_uni_Zscore, 'Delimiter', ';') 
dlmwrite('complete_factual_all_univ_betas_ROIs.csv', complete_uni_Zscore , 'Delimiter', ';')
dlmwrite('complete_counterfactual_all_univ_betas_ROIs.csv', counterfactual_uni_Zscore , 'Delimiter', ';')
