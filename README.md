Here you can find a description of the code and data associated with the manuscript:

Pischedda, D., Palminteri, S., & Coricelli, G. (2020). The Effect of Counterfactual Information on Outcome Value Coding in Medial Prefrontal and Cingulate Cortex: From an Absolute to a Relative Neural Code. The Journal of Neuroscience, 40(16), 3268-3277.

fMRI data reanalyzed in the manuscript are from the study in Palminteri et al., 2015.

REPOSITORY STRUCTURE
The repository contains the code and part of the data used to perform the analyses. Each script includes a description of its purpose/use, thus I only describe the content of each single folder below.

- Behavioral_analysis
  The directory contains R scripts used to perform the behavioral analyses described in the paper, the data used, and the output images. 

- fMRI_analysis
  The directory contains Matlab scripts to perform the fMRI analyses described in the manuscript. The scripts are organized in subfolders:
  - preprocessing: contains the code used to perform the preprocessing of the fMRI data
  - level1: contains the code used to perform the first level analyses  
  - svm: contains the code used to perform the MVPA analyses
  - level2: contains the code used to perform the second level analyses.  
  
- fMRI_data_extraction
  The directory contains Matlab scripts (*.m files) to extract mean values from betas and accuracy maps relative to the conditions of interest for each subject and some output files with the extracted values (*.csv files). The other results files are in the ROI_analysis folder. 
  
- ROI_analysis
  The directory contains R scripts used to perform the ROI analyses described in the paper, the data used, and the output images. 
